from random import shuffle
from collections import Counter, defaultdict

face_letters = {10:'X', 11:'J', 12:'Q', 13:'K', 14:'A'}
suit_icons = {1:'♣', 2:'♦', 3:'♥', 4:'♠'}

class Game:
    community = []
    hands = {}
    deck = []

    def __init__(self):
        self.deck = []
        # make deck
        for suit in range(1, 4+1):
            for value in range(2, 14+1):
                self.deck.append((value, suit))
        return

    def setup_new_game(self, hands, community):
        if len(hands.keys()) > 11: raise Exception('Too many players. 11 max.')
        if len(community) > 5: raise Exception('Too many cards in community. 5 max.')
        self.hands = hands
        self.community = community
        used_cards = set(community)
        for player_ in list(self.hands.keys()):
            used_cards.update(self.hands[player_])

        if len(self.deck) != 52:
            self.init()
        # remove used cards from deck
        self.deck = list(set(self.deck) - used_cards)

    def random_new_game(self, player_count):
        # make deck
        if player_count > 11: raise Exception('Too many players. 11 max.')
        if len(self.deck) != 52:
            self.init()

        shuffle(self.deck)
        
        # deal hands and flop
        for i in range(1, 1+player_count):
            self.hands[i] = [self.deck.pop(), self.deck.pop()]
        self.community = [self.deck.pop(), self.deck.pop(), self.deck.pop()]
        return

    def calc_player_odds(self):
        game_count = 0
        player_wins_dict = {}
        player_odds_dict = {}

        for player in list(self.hands.keys()):
            player_wins_dict[player] = 0
            # player_odds_dict[player] = 0
        for river_card in self.deck:
            temp_deck = self.deck + []
            temp_deck.remove(river_card)
            for turn_card in temp_deck:
                temp_community = self.community + [river_card]
                temp_community.append(turn_card)
                for player in self.calc_winning_hand(temp_community):
                    player_wins_dict[player] += 1
                game_count += 1
        for player in player_wins_dict:
            player_odds_dict[player] = player_wins_dict[player] / game_count
        return player_odds_dict

    def calc_winning_hand(self, temp_community):
        hand_type_dict = defaultdict(list)
        for player in self.hands:
            value_list = []
            suit_list = []
            hand = self.hands[player]
            full_hand = hand + temp_community
            hand_type = 0
            for card in full_hand:
                suit_list.append(card[1])
                value_list.append(card[0])
                value_list.sort()
            # Flush Test
            suit_count = Counter(suit_list)
            if suit_count.most_common(1)[0][1] == 5:
                hand_type = 5 #Flush
            # Straight Test
            i = 0
            for value in value_list:
                if i == 3: break
                i += 1
                l = [value, value + 1, value + 2, value + 3, value + 4]
                low_ace_l = [14, 2, 3, 4, 5]
                if all(value in value_list  for value in l) or all(value in value_list  for value in low_ace_l):
                    if l == {10, 11, 12, 13, 14} and hand_type == 5:
                        hand_type = 9 #Royal
                    elif hand_type == 5:
                        hand_type = 8 #Straight Flush
                    else:
                        hand_type = 4 #Straight
                    break
            # Multiples Test
            value_most_common = Counter(value_list).most_common(2)
            if hand_type not in [5,9,8,4]:
                if value_most_common[0][1] == 2:
                    if value_most_common[1][1] == 2:
                        hand_type = 2 #Two Pair
                    else:
                        hand_type = 1 #Pair
                if value_most_common[0][1] == 3:
                    if value_most_common[1][1] == 2:
                        hand_type = 6 #Full House
                    else:
                        hand_type = 3 #Three Kind
                if value_most_common[0][1] == 4:
                    hand_type = 7 #Four Kind
            # Store Hand Type
            hand_type_dict[hand_type].append((player, hand))
        # Compare Values
        victors = []
        hand_type_most_common = []
        for hand_type_ in hand_type_dict:
            hand_type_most_common.append((hand_type_, len(hand_type_dict[hand_type_])))
        hand_type_most_common.sort(reverse = True)
        if hand_type_most_common[0][1] == 1:
            victors = [hand_type_dict[hand_type_most_common[0][0]][0][0]]
        else:
            first_highest_dict = defaultdict(list)
            second_highest_dict = defaultdict(list)
            for player_ in hand_type_dict[hand_type_most_common[0][0]]:
                hand_ = sorted(player_[1], reverse = True)
                first_highest_dict[hand_[0][0]].append(player_[0])
                second_highest_dict[hand_[0][1]].append(player_[0])
            if len(first_highest_dict[sorted(list(first_highest_dict.keys()),reverse = True)[0]]) == 1:
                victors = first_highest_dict[sorted(list(first_highest_dict.keys()),reverse = True)[0]]
            elif len(second_highest_dict[sorted(list(second_highest_dict.keys()),reverse = True)[0]]) == 1:
                victors = second_highest_dict[sorted(list(second_highest_dict.keys()),reverse = True)[0]]
            else:
                victors = list(set(second_highest_dict[sorted(list(second_highest_dict.keys()),reverse = True)[0]]).intersection(first_highest_dict[sorted(list(first_highest_dict.keys()),reverse = True)[0]]))
        return victors

def card_to_str(card):
    card_string = ''
    if card[0] in face_letters.keys():
        card_string += face_letters[card[0]]
    else:
        card_string += str(card[0])
    card_string += suit_icons[card[1]]
    return card_string
